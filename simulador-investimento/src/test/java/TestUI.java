import core.DriverFactory;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import pages.ResultPage;
import pages.SimulatePage;
import util.Constant;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestUI {

    @Before
    public void init() {
        DriverFactory.getDriver();
    }

    @Rule
    public TestName testName = new TestName();

    @After
    public void finalize() throws IOException {
        takeScreenShot();
        DriverFactory.quitDriver();
    }

    private void takeScreenShot() throws IOException {
        TakesScreenshot ss = (TakesScreenshot) DriverFactory.getDriver();
        File file = ss.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(
                file,
                new File(
                        "target"
                                + File.separator
                                + "screenshot"
                                + File.separator
                                + testName.getMethodName()
                                + ".jpg"
                )
        );
    }

    @Test
    public void testFillAllValues() {
        SimulatePage simulate = new SimulatePage();
        simulate.fillAllValues(Constant.APPLY_VALUE_HIGHER_20, Constant.INVEST_VALUE_HIGHER_20, Constant.TIME);
        ResultPage result = simulate.clickSimulateButton();

        String expected = String.format("Em %s meses você terá guardado", Constant.TIME);
        assertEquals(expected, result.getResultMessage());
    }

    @Test
    public void testValidateAllValues() {
        SimulatePage simulate = new SimulatePage();

        simulate.fillApplyValue(Constant.APPLY_VALUE_LESS_20);
        simulate.clickSimulateButton();
        assertEquals("Valor mínimo de 20.00", simulate.getApplyValueErrorMessage());

        simulate.fillInvestValue(Constant.INVEST_VALUE_LESS_20);
        simulate.clickSimulateButton();
        assertEquals("Valor mínimo de 20.00", simulate.getInvestValueErrorMessage());

        simulate.fillTime(Constant.TIME_NULL);
        simulate.clickSimulateButton();
        assertEquals("Obrigatório", simulate.getTimeErrorMessage());
    }

    @Test
    public void testCleanData() {
        SimulatePage simulate = new SimulatePage();
        simulate.fillAllValues(Constant.APPLY_VALUE_HIGHER_20, Constant.INVEST_VALUE_HIGHER_20, Constant.TIME);
        simulate.clickCleanButton();
        assertEquals("", simulate.getApplyValueInput());
        assertEquals("", simulate.getInvestValueInput());
        assertEquals("", simulate.getTimeInput());
    }

    @Test
    public void testGoToResultAndReturnToSimulate() {
        SimulatePage simulateOne = new SimulatePage();
        simulateOne.fillAllValues(Constant.APPLY_VALUE_HIGHER_20, Constant.INVEST_VALUE_HIGHER_20, Constant.TIME);
        ResultPage result = simulateOne.clickSimulateButton();
        assertTrue(result.existResultTable());

        SimulatePage simulateTwo = result.clickBackButton();
        assertTrue(simulateTwo.isSimulatePage());
    }
}









