import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Executar casos de teste em lote
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestUI.class
})
public class TestSuite {
}
