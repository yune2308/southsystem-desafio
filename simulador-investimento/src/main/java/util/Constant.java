package util;

public class Constant {

    // CHROME DRIVER
    public static final String URL = "https://www.sicredi.com.br/html/ferramenta/simulador-investimento-poupanca/";
    public static final String CHROME_DRIVER = "webdriver.chrome.driver";
    public static final String DRIVER_PATH = "src/main/resources/chromedriver.exe";

    // TEST
    public static final Integer APPLY_VALUE_HIGHER_20 = 2500;
    public static final Integer APPLY_VALUE_LESS_20 = 1000;
    public static final Integer INVEST_VALUE_HIGHER_20 = 2500;
    public static final Integer INVEST_VALUE_LESS_20 = 1000;
    public static final Integer TIME = 4;
    public static final Integer TIME_NULL = null;
}
