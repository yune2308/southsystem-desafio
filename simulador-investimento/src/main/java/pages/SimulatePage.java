package pages;

import core.DriverFactory;
import org.openqa.selenium.By;

public class SimulatePage {

    // Formulário de simulação
    private final By form = By.id("formInvestimento");
    // Perfil
    private final By profileForMeRadio = By.xpath("//input[@name='perfil'][1]");
    private final By profileForMyCompanyRadio = By.xpath("//input[@name='perfil'][2]");
    // Valor aplicar
    private final By applyValueInput = By.id("valorAplicar");
    // Valor poupar
    private final By investValueInput = By.id("valorInvestir");
    // Tempo poupar
    private final By timeInput = By.id("tempo");
    // Button simular
    private final By simulateButton = By.xpath("//button[@type='submit']");
    // Link limpar formulário
    private final By cleanButton = By.cssSelector(".btnLimpar");
    // Mensagens de erro
    private final By applyValueErrorMessage = By.id("valorAplicar-error");
    private final By investValueErrorMessage = By.id("valorInvestir-error");
    private final By timeErrorMessage = By.id("tempo-error");

    public void selectForMeProfile(){
        DriverFactory.getDriver().findElement(profileForMeRadio).click();
    }

    public void selectForCompanyProfile(){
        DriverFactory.getDriver().findElement(profileForMyCompanyRadio).click();
    }

    public void fillApplyValue(Integer applyValue) {
        DriverFactory.getDriver().findElement(applyValueInput).sendKeys(applyValue == null ? "" : applyValue.toString());
    }

    public void fillInvestValue(Integer investValue) {
        DriverFactory.getDriver().findElement(investValueInput).sendKeys(investValue == null ? "" : investValue.toString());
    }

    public void fillTime(Integer time) {
        DriverFactory.getDriver().findElement(timeInput).sendKeys(time == null ? "" : time.toString());
    }

    public ResultPage clickSimulateButton() {
        DriverFactory.getDriver().findElement(simulateButton).submit();
        return new ResultPage();
    }

    public void clickCleanButton() {
        DriverFactory.getDriver().findElement(cleanButton).click();
    }

    public String getApplyValueInput() {
        return DriverFactory.getDriver().findElement(applyValueInput).getText();
    }

    public String getApplyValueErrorMessage() {
        return DriverFactory.getDriver().findElement(applyValueErrorMessage).getText();
    }

    public String getInvestValueInput() {
        return DriverFactory.getDriver().findElement(investValueInput).getText();
    }

    public String getInvestValueErrorMessage() {
        return DriverFactory.getDriver().findElement(investValueErrorMessage).getText();
    }

    public String getTimeInput() {
        return DriverFactory.getDriver().findElement(timeInput).getText();
    }

    public String getTimeErrorMessage() {
        return DriverFactory.getDriver().findElement(timeErrorMessage).getText();
    }

    public boolean isSimulatePage() {
        return DriverFactory.getDriver().findElement(form).isDisplayed();
    }

    public void fillAllValues(Integer applyValue, Integer investValue, Integer time) {
        selectForMeProfile();
        fillApplyValue(applyValue);
        fillInvestValue(investValue);
        fillTime(time);
    }
}

