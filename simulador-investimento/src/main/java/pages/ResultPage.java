package pages;

import core.DriverFactory;
import org.openqa.selenium.By;

public class ResultPage {

    // Message
    private final By resultMessage = By.xpath("//div/div/div[2]/span");
    // Table
    private final By resultTable = By.xpath("//table");
    // Button back
    private final By backButton = By.cssSelector(".btnRefazer");

    public SimulatePage clickBackButton() {
        DriverFactory.getDriver().findElement(backButton).click();
        return new SimulatePage();
    }

    public String getResultMessage() {
        DriverFactory.waitDriver(resultMessage);
        return DriverFactory.getDriver().findElement(resultMessage).getText();
    }

    public boolean existResultTable() {
        DriverFactory.waitDriver(resultTable);
        return DriverFactory.getDriver().findElement(resultTable).isDisplayed();
    }
}
